#!/bin/sh -e
pomof=/tmp/doing-pomo
AKRDIR=$HOME/.akr
lockfile="$AKRDIR/lock"
mpvc='mpv -quiet --volume=50 ~/radio/default.pls'
musicsession=pomus

music_start() {
	if tmux has-session -t $musicsession 2>/dev/null; then
		tmux kill-session -t $musicsession
	fi
	tmux new-session -s $musicsession -d "$mpvc; exec $SHELL"
}

music_stop() {
	tmux kill-session -t $musicsession
}

file_younger_than() {
	[ -f "$1" ] || return 1
	[ "$(date -r "$1" +%s)" -ge "$(date -d "$2 ago" +%s)" ]
}

closeworkspaces() {
	swaymsg for_window '[workspace="[1-7]"]' opacity set 0.1 || true
	swaymsg '[workspace="[1-7]"]' opacity set 0.1 || true
}

openworkspaces() {
	swaymsg for_window '[workspace="[1-7]"]' opacity set 1 || true
	swaymsg '[workspace="[1-7]"]' opacity set 1 || true
}

run() {
	mins="${1:-25}"
	if [ -f $pomof ]; then
		echo $pomof exists
		exit 1
	fi
	if [ -f "$lockfile" ]; then
		if file_younger_than $lockfile '30 minutes'; then
			echo lock
			exit
		fi
	fi
	if [ "$(bluetoothctl devices Connected)" = ""  ]; then
		echo "no headphones"
		exit
	fi
	touch $pomof
	atend() {
		rm $pomof
		music_stop
		echo 'release keys'
		sleep 0.5
		swaymsg reload
		exit 0
	}
	trap atend INT
	music_start
	openworkspaces
	date
	sleep ${mins}m
	swaynag -m end
}

cmd="$1"
shift
case "$cmd" in
	run) run;;
	refresh)
		if file_younger_than $pomof '30 minutes'; then
			openworkspaces
		else
			closeworkspaces
		fi;;
	*) echo unknown command;;
esac
